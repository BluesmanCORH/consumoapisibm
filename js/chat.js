function responsiveChat(element) {
    $(element).html('<form class="chat"><span></span><div class="messages"></div><input type="text" placeholder="Your message..."><button id="btnProfile" title="Profile" onclick="window.location.href=\'./personality.html\'"><span id="iconProfile" class="glyphicon glyphicon-user"></span></button><button id="btnFeedback" title="Feedback" data-toggle="modal" data-target="#myModal"><span id="iconFeedback" class="glyphicon glyphicon-list-alt"></span></button><hr style="width:5px; display:inline-block;"><button id="btnSend" title="Send"><span id="iconSend" class="glyphicon glyphicon-send"></span></button><input type="submit" value="Send"></form>');

    function showLatestMessage() {
        $(element).find('.messages').scrollTop($(element).find('.messages').height());
    }
    showLatestMessage();

    $(element + ' input[type="text"]').keypress(function (event) {
        if (event.which == 13) {
            event.preventDefault();
            $(element + ' input[type="submit"]').click();
        }
    });

    $(element + ' button').click(function (event) {
        event.preventDefault();
        $(element + ' input[type="submit"]').click();
    });

    $(element + ' input[type="submit"]').click(function (event) {
        event.preventDefault();
        var message = $(element + ' input[type="text"]').val();
        if ($(element + ' input[type="text"]').val()) {
            $(element + ' div.messages').append(
                '<div class="message"><div class="myMessage"><p>' +
                message +
                "</p><date>" +
                getCurrentDate() +
                "</date></div></div>"
            );
            setTimeout(function () {
                $(element + ' > span').addClass("spinner");
            }, 100);
            setTimeout(function () {
                $(element + ' > span').removeClass("spinner");
            }, 2000);
        }
        sendChatbotMessage(message);
        callNaturalLanguageUnderstandingAPI(message);
        $(element + ' input[type="text"]').val("");
        showLatestMessage();
    });
}

function responsiveChatPush(element, sender, origin, date, message) {
    var originClass;
    if (origin == 'me') {
        originClass = 'myMessage';
    } else {
        originClass = 'fromThem';
    }
    $(element + ' .messages').append('<div class="message"><div class="' + originClass + '"><p>' + message + '</p><date><b>' + sender + '</b> ' + date + '</date></div></div>');
}

function sendMessageAsReceiver(message) {
    responsiveChatPush('.chat', 'Chatbot', 'you', getCurrentDate(), message);
    if(document.getElementById("receiverMsg") != null)	
    	document.getElementById("receiverMsg").value = "";
}

function getCurrentDate() {
    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
        "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
    ];

    var today = new Date();
    var dd = today.getDate().toString().padStart(2, '0');
    var MM = monthNames[today.getMonth()];
    var yyyy = today.getFullYear().toString().padStart(4, '0');
    var HH = today.getHours().toString().padStart(2, '0');
    var mm = today.getMinutes().toString().padStart(2, '0');
    var ss = today.getSeconds().toString().padStart(2, '0');

    return MM + ' ' + dd + ', ' + yyyy + " - " + HH + ":" + mm + ":" + ss;
}

$(document).ready(() => {

    fetch("https://chatbotwatsonspr.herokuapp.com/chat/createSession", {
        method: 'GET',

    }).then((response) => {
        if (response.ok) {
            console.log("OK RESPONSE")
            return response.json();
        }
    }).then(data => {
        let sessionId = data["session"];
        sendMessageAsReceiver("Session id:" + sessionId);
        sendMessageAsReceiver("How can i help you?");

    }).catch(() => {
        sendMessageAsReceiver("An error occurs in spring boot service")
    })
})

sendChatbotMessage = (message) => {
    fetch("https://chatbotwatsonspr.herokuapp.com/chat/conversation/" + message, {
        method: 'GET',

    }).then((response) => {
        if (response.ok) {
            console.log("OK RESPONSE")
            return response.json();
        }
    }).then(data => {
        let message = data["message"];
        if (data["callEvents"] == true) {
            callEvents();
        }
        sendMessageAsReceiver(message);
    })
}

callEvents = () => {
    fetch("https://chatbotwatsonspr.herokuapp.com/chat/events", {
        method: 'GET',

    }).then((response) => {
        if (response.ok) {
            console.log("OK RESPONSE")
            return response.json();
        }
    }).then(data => {
        console.log(data);
        data.map((event) => {
            let result = "Summary: " + event.summary + " \n Start: " + event.start + " \n End: " + event.end + " \n Description: " + event.description + " \n Location: " + event.location;
            sendMessageAsReceiver(result);

        })
        sendMessageAsReceiver(message);
    })
}

callNaturalLanguageUnderstandingAPI = (message) => {
    fetch('http://natural-language-understanding.herokuapp.com/NaturalLanguageUnderstanding/' + message, {
        method: "get",
        //mode: 'no-cors',
    })
        .then(response => {
            return response.json();
        })
        .then(data => {
            console.log(data);
        })
        .catch(err => {
            console.log("Upps... There was an error in the callNaturalLanguageUnderstandingAPI method");
        })
}
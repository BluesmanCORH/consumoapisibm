function listDevices1() {
    $('#textarea1').val("");
    fetch('https://softtek-iot-platform.herokuapp.com/iot/listDevices', {
        method: "get",
        //mode: 'no-cors',
    })
        .then(response => {
            return response.json();
        })
        .then(data => {
            console.log(data);
            var txt = "";
            for (var i = 0; i < data.results.length; i++) {
                txt += (i + 1) + " > clientId: \"" + data.results[i].clientId + "\",\n"
                    + "      typeId: \"" + data.results[i].typeId + "\",\n"
                    + "      deviceId: \"" + data.results[i].deviceId + "\"\n\n";
            }
            $('#textarea1').val(txt);
        })
        .catch(err => {
            console.log("Upps... There was an error in the listDevices1 method");
            $('#textarea1').val("Upss... There was a problem. Could you try it again please?");
        })
}

function getStateDevice1() {
    $('#textarea1').val("");
    fetch('https://softtek-iot-platform.herokuapp.com/iot/getStateDevice', {
        method: "get",
        //mode: 'no-cors',
    })
        .then(response => {
            return response.json();
        })
        .then(data => {
            console.log(data);
            $('#textarea1').val("cont: \"" + data.cont + "\"");
        })
        .catch(err => {
            console.log("Upps... There was an error in the getStateDevice1 method");
            $('#textarea1').val("Upss... There was a problem. Could you try it again please?");
        })
}

function resetStateDevice() {
    $('#textarea1').val("");
    fetch('https://softtek-iot-platform.herokuapp.com/iot/resetStateDevice', {
        method: "get",
        //mode: 'no-cors',
    })
        .then(response => {
            return response.json();
        })
        .then(data => {
            console.log(data);
            $('#textarea1').val("message: \"" + data.message + "\"");
        })
        .catch(err => {
            console.log("Upps... There was an error in the resetStateDevice method");
            $('#textarea1').val("Upss... There was a problem. Could you try it again please?");
        })
}

function listDevices2() {
    $('#textarea2').val("");
    fetch('https://iotsofttektrai.herokuapp.com/iot/listDevices', {
        method: "get",
        //mode: 'no-cors',
    })
        .then(response => {
            return response.json();
        })
        .then(data => {
            console.log(data);
            var txt = "";
            for (var i = 0; i < data.results.length; i++) {
                txt += (i + 1) + " > clientId: \"" + data.results[i].clientId + "\",\n"
                    + "      typeId: \"" + data.results[i].typeId + "\",\n"
                    + "      deviceId: \"" + data.results[i].deviceId + "\"\n\n";
            }
            $('#textarea2').val(txt);
        })
        .catch(err => {
            console.log("Upps... There was an error in the listDevices2 method");
            $('#textarea2').val("Upss... There was a problem. Could you try it again please?");
        })
}

function getStateDevice2() {
    $('#textarea2').val("");
    fetch('https://iotsofttektrai.herokuapp.com/iot/getStateDevice', {
        method: "get",
        //mode: 'no-cors',
    })
        .then(response => {
            return response.json();
        })
        .then(data => {
            console.log(data);
            $('#textarea2').val("randomNumber: \"" + data.randomNumber + "\"");
        })
        .catch(err => {
            console.log("Upps... There was an error in the getStateDevice2 method");
            $('#textarea2').val("Upss... There was a problem. Could you try it again please?");
        })
}
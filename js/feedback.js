



(
    () => {
        $(document).ready(() => {

            function sleep(ms) {
                return new Promise(resolve => setTimeout(resolve, ms));
              }

            var mediaRecorder;
            console.log("this is the feedback")
            const btnRecord = document.getElementById('btnRecord');
            console.log("this is the feedback")

            btnRecord.addEventListener('click', function (event) {

                event.preventDefault();

                const circleRecord = document.getElementById('circleRecord');

                if (btnRecord.innerText === "Stop record") {

                    mediaRecorder.stop();
                    this.innerHTML = "Record audio to sent your feedback";
                    circleRecord.style.color = "#1abc9c";

                } else {

                    this.innerHTML = "Stop record"

                    navigator.mediaDevices.getUserMedia({ audio: true })
                        .then(stream => {

                            mediaRecorder = new MediaRecorder(stream);
                            mediaRecorder.start();

                            const audioChunks = [];

                            mediaRecorder.addEventListener("dataavailable", event => {
                                audioChunks.push(event.data);
                            });

                            MediaRecorder.isTypeSupported("audio/wav;codecs=MS_PCM")

                            mediaRecorder.addEventListener("stop", async () => {

                                const audioBlob = new Blob(audioChunks, { 'type': 'audio/wav; codecs=MS_PCM' });
                                const audioUrl = URL.createObjectURL(audioBlob);

                                const audio = new Audio(audioUrl);

                                audio.play();

                                const reader = new FileReader();

                                let base64data;

                                reader.readAsDataURL(audioBlob);
                                reader.onloadend = async function () {
                                    base64data = reader.result;

                                    const data = new FormData();


                                    data.append("file", base64data);


                                    const r = await fetch("https://still-springs-71333.herokuapp.com/speech/transcribe", {
                                        body: data,
                                        method: 'POST'
                                    });

                                    const textInput = document.getElementById("ouputText");

                                    if (r.status === 200) {
                                        const result = await r.json();
                                        console.log(result);
                                        if (result.results.length > 0) {
                                            if (result.results[0].alternatives.length > 0) {
                                                const text = result.results[0].alternatives[0].transcript
                                                console.log(text)       
                                            }
                                        }

                                        textInput.innerHTML = "Your comments have been sent successfully";

                                        await sleep(5000);

                                        textInput.innerHTML = "";
                                        

                                    }

                                }
                            });

                            circleRecord.style.color = "#dc3545";

                        }).catch((e) => {
                            
                            this.innerHTML = "Record audio to sent your feedback";

                        });
                }

            }, false);

        })


    }

)();
